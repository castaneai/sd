import torch
import diffusers

device = "cuda" if torch.cuda.is_available() else "mps"
dtype = torch.float16 if device == "cuda" else torch.float32
print(f"device: {device}, dtype: {dtype}")

# model
vae = diffusers.AutoencoderKL.from_pretrained(
    "models/vae/Replicant-V1.0", torch_dtype=dtype)
pipe = diffusers.StableDiffusionPipeline.from_pretrained(
    "models/Replicant-V1.0", torch_dtype=dtype, vae=vae)
pipe.scheduler = diffusers.EulerAncestralDiscreteScheduler.from_config(
    pipe.scheduler.config, use_karras_sigmas=True)
pipe = pipe.to(device)
# pipe.load_textual_inversion(
#     "models/embeddings/re-badprompt.safetensors", use_safetensors=True)
prompts = ["1girl"]
negative_prompts = [
    "re-badprompt,missing finger, extra digits, fewer digits,((mutated hands and fingers)),"]
steps = 20
width = 512
height = 768
seed = 1
guidance_scale = 7.5
# randn does not work reproducibly on mps
gen_device = "cpu" if device == "mps" else device
generator = torch.Generator(device=gen_device)
generator.manual_seed(seed)

output = pipe(prompts, negative_prompt=negative_prompts, width=width,
              height=height, num_inference_steps=steps, guidance_scale=guidance_scale, generator=generator)
output.images[0].save("output.png")
